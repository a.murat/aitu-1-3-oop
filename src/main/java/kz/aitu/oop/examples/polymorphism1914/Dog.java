package kz.aitu.oop.examples.polymorphism1914;

public class Dog extends Animal {

    public Dog(String name) {
        super(name);
    }

    public void run() {
        System.out.println("Dog " + this.getName() + " runs...");
    }

    public void eat() {
        System.out.println("Dog " + this.getName() + " eats...");
    }

}
